package shulyak.com.wakeme

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.NavigationView
import android.support.design.widget.Snackbar
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.TypedValue
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import com.fasterxml.jackson.databind.ObjectMapper
import com.github.kittinunf.fuel.android.extension.responseJson
import com.github.kittinunf.fuel.httpGet
import org.json.JSONArray
import shulyak.com.wakeme.adapters.AlarmsListAdapter
import shulyak.com.wakeme.entities.Alarm
import shulyak.com.wakeme.entities.User

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    private var user: User? = null

    private var alarmsList: RecyclerView? = null
    private var mAdapter: AlarmsListAdapter? = null
    private var swipeRefresh: SwipeRefreshLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)

        user = intent.getParcelableExtra("user")

        val fab = findViewById(R.id.fab) as FloatingActionButton
        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }

        val drawer = findViewById(R.id.drawer_layout) as DrawerLayout
        val toggle = ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer.addDrawerListener(toggle)
        toggle.syncState()

        val navigationView = findViewById(R.id.nav_view) as NavigationView
        navigationView.setNavigationItemSelectedListener(this)

        val navigationViewHeader = navigationView.getHeaderView(0)

        val drawerHeaderFullName = navigationViewHeader.findViewById(R.id.nav_header_main_full_name) as TextView
        drawerHeaderFullName.text = user!!.fullName

        val drawerHeaderEmail = navigationViewHeader.findViewById(R.id.nav_header_main_email) as TextView
        drawerHeaderEmail.text = user!!.email

        val addAlarmFab = findViewById(R.id.fab)
        addAlarmFab.setOnClickListener {
            val intent = Intent(this, AlarmActivity::class.java)
            intent.putExtra("user", user)
            startActivity(intent)
        }

        alarmsList = findViewById(R.id.alarm_list) as RecyclerView
        val linearLayoutManager = LinearLayoutManager(this)
        alarmsList!!.layoutManager = linearLayoutManager

        swipeRefresh = findViewById(R.id.swipe_refresh) as SwipeRefreshLayout
        swipeRefresh!!.setProgressViewOffset(false, 0,
                TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 24f, resources.displayMetrics).toInt())
        swipeRefresh!!.setOnRefreshListener(SwipeRefreshLayout.OnRefreshListener { fetchAlarms() })

        fetchAlarms()
    }

    private fun fetchAlarms() {
//        "http://169.254.140.215:8080/api/login".httpGet()
//                .header(Pair("X-Auth-Token", credentials))
//        val view = findViewById(android.R.id.content)
//        val alarms = mutableSetOf<Alarm>(*this.user?.alarms?.toTypedArray())

        val alarms = arrayListOf<Alarm>()
        val apiUrl = getString(R.string.api_url)
        "${apiUrl}/users/${user?.id}/alarms".httpGet()
                .responseJson { _, _, result ->
                    result.fold(
                            { json ->
                                val alarmArray = JSONArray(json.content)
                                for (i in 0..alarmArray.length() - 1) {
                                    val jsonObject = alarmArray.getJSONObject(i)
                                    val alarm = ObjectMapper().readValue(jsonObject.toString(), Alarm::class.java)
                                    alarms.add(alarm)
                                }
                                mAdapter = AlarmsListAdapter(alarms)
                                alarmsList?.setAdapter(mAdapter)
                                swipeRefresh?.isRefreshing = false
                            },
                            { fuelError ->
                                val view = findViewById(android.R.id.content)
                                Snackbar.make(view, "Failed to fetch alarms", Snackbar.LENGTH_LONG)
                                        .setAction("Action", null).show()
                            }
                    )
                }
    }

    override fun onBackPressed() {
        val drawer = findViewById(R.id.drawer_layout) as DrawerLayout
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_all_alarms -> {
                // Handle the camera action
            }
            R.id.nav_active_alarms -> {

            }
            R.id.nav_disabled_alarms -> {

            }
            R.id.nav_logout -> {

            }
//            R.id.nav_share -> {
//
//            }
//            R.id.nav_send -> {
//
//            }
        }

        val drawer = findViewById(R.id.drawer_layout) as DrawerLayout
        drawer.closeDrawer(GravityCompat.START)
        return true
    }
}
