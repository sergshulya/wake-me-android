package shulyak.com.wakeme.adapters

import android.content.Intent
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Switch
import android.widget.TextView
import shulyak.com.wakeme.AlarmActivity
import shulyak.com.wakeme.R
import shulyak.com.wakeme.entities.Alarm
import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.util.*


internal class AlarmsListAdapter(private val alarms: ArrayList<Alarm>) : RecyclerView.Adapter<AlarmsListAdapter.AlarmViewHolder>() {
    private val clickListner: View.OnClickListener = View.OnClickListener { view ->
            val alarmId = view.getTag(R.id.alarms_card_view) as Long
//            val time = view.getTag(R.id.alarm_time_text_view) as String
//            val label = view.getTag(R.id.alarm_label_text_view) as String
            val context = view.context

            val intent = Intent(context, AlarmActivity::class.java)
            intent.putExtra("id", alarmId)
//            intent.putExtra("user", user)
//            intent.putExtra("time", time)
            intent.putExtra("alarm", alarms.find { a -> a.id == alarmId });
            context.startActivity(intent)
    }

    internal inner class AlarmViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var cardView: CardView
        var labelTextView: TextView
        var timeTextView: TextView
        var enabledSwitch: Switch

        init {
            cardView = itemView.findViewById(R.id.alarms_card_view) as CardView
            timeTextView = itemView.findViewById(R.id.alarm_time_text_view) as TextView
            labelTextView = itemView.findViewById(R.id.alarm_label_text_view) as TextView
            enabledSwitch = itemView.findViewById(R.id.alarm_enabled_switch) as Switch
            enabledSwitch.textOff = "Off"
            enabledSwitch.textOn = "On"

            enabledSwitch.setOnClickListener {
                alarms
            }

//            clickListner = ProfileClickListner()
//            cardView.setOnClickListener(clickListner)
        }
    }

    fun update(data: ArrayList<Alarm>) {
        alarms.clear()
        alarms.addAll(data)
        notifyDataSetChanged()
    }

//    internal inner class ProfileClickListner : View.OnClickListener {
//        override fun onClick(view: View) {
//
//            val alarmId = view.getTag(R.id.alarms_card_view) as String
//            val time = view.getTag(R.id.alarm_time_text_view) as String
//            val label = view.getTag(R.id.alarm_label_text_view) as String
//            val context = view.context
//
//            val intent = Intent(context, AlarmActivity::class.java)
////            intent.putExtra("id", alarmId)
////            intent.putExtra("time", time)
//            intent.putExtra("alarm", alarms.find { a -> a.id == alarmId.toLong() });
//            context.startActivity(intent)
//        }
//    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlarmViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.card_list_alarm, parent, false)
        return AlarmViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: AlarmViewHolder, position: Int) {
        val alarm = alarms[position]

        val labelTextView = holder.labelTextView
        labelTextView.text = alarm.label
        val timeTextView = holder.timeTextView

        val timestamp = Timestamp(alarm.time.toLong())
        val formattedTime = SimpleDateFormat("HH:mm").format(timestamp)

        timeTextView.text = formattedTime
        val enabledSwitch = holder.enabledSwitch
        enabledSwitch.isChecked = alarm.isEnabled
        enabledSwitch.text = if (alarm.isEnabled) "On" else "Off"

        holder.cardView.setTag(R.id.alarms_card_view, alarm.id);
//        holder.cardView.setTag(R.id.label_text_edit, cardHeaderText);
        holder.cardView.setOnClickListener(clickListner);
    }

    override fun getItemCount(): Int {
        return alarms.size
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView?) {
        super.onAttachedToRecyclerView(recyclerView)
    }
}
