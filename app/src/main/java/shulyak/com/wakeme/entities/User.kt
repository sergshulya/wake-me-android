package shulyak.com.wakeme.entities

import android.os.Parcel
import android.os.Parcelable

@com.fasterxml.jackson.annotation.JsonIgnoreProperties(ignoreUnknown = true)
class User(
        var id: Long = -1,
        var email: String = "",
        var password: String = "",
        var fullName: String = "",
        var token: String = "",
        var alarms: ArrayList<Alarm> = arrayListOf<Alarm>()) : Parcelable {
    companion object {
        @JvmField val CREATOR: Parcelable.Creator<User> = object : Parcelable.Creator<User> {
            override fun createFromParcel(source: Parcel): User = User(source)
            override fun newArray(size: Int): Array<User?> = arrayOfNulls(size)
        }
    }

    constructor(source: Parcel) : this(
            source.readLong(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.createTypedArrayList(Alarm.CREATOR)
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeLong(id)
        dest.writeString(email)
        dest.writeString(password)
        dest.writeString(fullName)
        dest.writeString(token)
        dest.writeTypedList(alarms)
    }
}