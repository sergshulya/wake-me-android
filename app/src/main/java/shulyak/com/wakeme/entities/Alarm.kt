package shulyak.com.wakeme.entities

import android.os.Parcel
import android.os.Parcelable

@com.fasterxml.jackson.annotation.JsonIgnoreProperties(ignoreUnknown = true)
class Alarm(var id: Long? = null, var time: String = "", var label: String = "", var isEnabled: Boolean = false) : Parcelable {
    companion object {
        @JvmField val CREATOR: Parcelable.Creator<Alarm> = object : Parcelable.Creator<Alarm> {
            override fun createFromParcel(source: Parcel): Alarm = Alarm(source)
            override fun newArray(size: Int): Array<Alarm?> = arrayOfNulls(size)
        }
    }

    constructor(source: Parcel) : this(
            source.readLong(),
            source.readString(),
            source.readString(),
            1 == source.readInt()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) {
        id?.let { dest.writeLong(it) }
        dest.writeString(time)
        dest.writeString(label)
        dest.writeInt((if (isEnabled) 1 else 0))
    }

    override fun toString(): String {
        return "Alarm(id=$id, time='$time', label='$label', isEnabled=$isEnabled)"
    }
}