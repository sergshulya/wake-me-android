package shulyak.com.wakeme

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.EditText
import com.github.kittinunf.fuel.android.extension.responseJson
import com.github.kittinunf.fuel.httpPost
import com.google.gson.Gson
import shulyak.com.wakeme.entities.Alarm
import shulyak.com.wakeme.entities.User
import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.util.*
import java.util.Calendar.HOUR
import java.util.Calendar.MINUTE


class AlarmActivity : AppCompatActivity() {
    private var timeInput: EditText? = null
    private var labelInput: EditText? = null
    private var alarm: Alarm? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_alarm)
        timeInput = findViewById(R.id.time_text_edit) as EditText
        labelInput = findViewById(R.id.label_text_edit) as EditText

        alarm = intent.getParcelableExtra("alarm")

        val saveButton = findViewById(R.id.alarm_save_button) as Button

        if (alarm != null) {
            val timestamp = Timestamp(alarm!!.time.toLong())
            val formattedTime = SimpleDateFormat("HH:mm").format(timestamp)
            timeInput!!.setText(formattedTime)
            labelInput!!.setText(alarm?.label)

            saveButton.setOnClickListener({ onUpdateClickListener(alarm!!) })
        } else {
            saveButton.setOnClickListener { onClickListener() }
        }
    }

    private fun onUpdateClickListener(alarm: Alarm) {
        alarm.label = labelInput!!.text.toString()
        val (hours, minutes) = (timeInput!!.text.toString()).split(":")
        val calendar = Calendar.getInstance()
        calendar.set(HOUR, hours.toInt())
        calendar.set(MINUTE, minutes.toInt())
        alarm.time = calendar.timeInMillis.toString()

        alarm.isEnabled = alarm.isEnabled

        val user = intent.getParcelableExtra<User>("user")

        updateAlarm(user, alarm)
    }

    private fun onClickListener() {
        val alarm = Alarm()
        alarm.label = labelInput!!.text.toString()
        val (hours, minutes) = (timeInput!!.text.toString()).split(":")
        val calendar = Calendar.getInstance()
        calendar.set(HOUR, hours.toInt())
        calendar.set(MINUTE, minutes.toInt())
        alarm.time = calendar.timeInMillis.toString()

        alarm.isEnabled = false

        val json = Gson().toJson(alarm)

        val user = intent.getParcelableExtra<User>("user")

        saveAlarm(user, json)
    }

    private fun saveAlarm(user: User, alarmJson: String) {
        "http://169.254.140.215:8080/api/users/${user.id}/alarms".httpPost()
                .body(alarmJson)
                .header("Content-Type" to "application/json")
                .responseJson { _, _, result ->
                    result.fold(
                            {
                                returnToMainActivity(user)
                            },
                            {
                                showError()
                            }
                    )
                }
    }

    private fun updateAlarm(user: User, alarm: Alarm) {
        val apiUrl = getString(R.string.api_url)
        "${apiUrl}/users/${user.id}/alarms/${alarm.id}".httpPost()
                .body(Gson().toJson(alarm))
                .header("Content-Type" to "application/json")
                .responseJson { _, _, result ->
                    result.fold(
                            {
                                returnToMainActivity(user)
                            },
                            {
                                showError()
                            }
                    )
                }
    }

    private fun showError() {
        val view = findViewById(android.R.id.content)
        Snackbar.make(view, "Failed to create alarm", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
    }

    private fun returnToMainActivity(user: User) {
        val intent = Intent(this, MainActivity::class.java)
        intent.putExtra("user", user)
        startActivity(intent)
    }
}
